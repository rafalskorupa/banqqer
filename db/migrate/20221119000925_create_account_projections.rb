class CreateAccountProjections < ActiveRecord::Migration[7.0]
  def change
    create_table :account_projections do |t|
      t.string :status
      t.string :account_number
      t.string :balance
      t.datetime :opened_at

      t.timestamps
    end
  end
end
