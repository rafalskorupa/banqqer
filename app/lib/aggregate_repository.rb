# frozen_string_literal: true

class AggregateRepository
  def initialize(event_store = Rails.configuration.event_store)
    @repository = AggregateRoot::Repository.new(event_store)
  end

  def self.aggregate(aggregate_class)
    define_method(:aggregate_class) do
      aggregate_class
    end
  end

  def get(id)
    repository.load(aggregate_class.new, stream_name(id))
  end

  def with_aggregate(id, &block)
    # TODO: Verify it uses optimistic locking
    repository.with_aggregate(aggregate_class.new, stream_name(id), &block)
  end

  def stream_name(id)
    aggregate_class
      .to_s
      .split("::")
      .excluding("Aggregate")
      .append(id)
      .join("#")
      .downcase
  end

  protected

  attr_reader :repository
end
