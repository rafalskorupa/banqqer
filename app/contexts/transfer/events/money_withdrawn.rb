# frozen_string_literal: true

module Transfer
  module Events
    class MoneyWithdrawn < RailsEventStore::Event; end
  end
end
