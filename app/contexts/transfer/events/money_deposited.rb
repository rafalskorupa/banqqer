# frozen_string_literal: true

module Transfer
  module Events
    class MoneyDeposited < RailsEventStore::Event; end
  end
end
