# frozen_string_literal: true

module Transfer
  class Repository < AggregateRepository
    aggregate Transfer::Aggregate
  end
end
