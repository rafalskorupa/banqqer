# frozen_string_literal: true

module Transfer
  module Commands
    class DepositMoney
      def self.call!(account_number, amount)
        new.call!(account_number, amount)
      end

      def call!(account_number, amount)
        transfer_id = SecureRandom.uuid

        account_repository.with_aggregate(account_number) do |account|
          transfer_repository.with_aggregate(transfer_id) do |transfer|
            transfer.deposit(transfer_id, account_number, amount)
            account.transfer(transfer_id, amount)
          end
        end
      end

      private

      def transfer_repository
        @transfer_repository = Transfer::Repository.new
      end

      def account_repository
        @account_repository = Account::Repository.new
      end
    end
  end
end
