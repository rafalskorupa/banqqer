# frozen_string_literal: true

module Transfer
  class Aggregate
    include AggregateRoot

    class AmountNotInteger < StandardError; end
    class AmountMustBePositive < StandardError; end

    attr_reader :type, :source, :target, :status, :id

    def initialize
      @status = :initialized
    end

    def deposit(id, account_number, amount)
      raise AmountNotInteger unless amount.is_a?(Integer)
      raise AmountMustBePositive unless amount.positive?

      apply Transfer::Events::MoneyDeposited.new(
        data: {
          id: id,
          target: { account_number: account_number },
          amount: amount,
          requested_at: DateTime.now.iso8601
        }
      )
    end

    def withdraw(id, account_number, amount)
      raise AmountNotInteger unless amount.is_a?(Integer)
      raise AmountMustBePositive unless amount.positive?

      apply Transfer::Events::MoneyWithdrawn.new(
        data: {
          id: id,
          source: { account_number: account_number },
          amount: amount,
          requested_at: DateTime.now.iso8601
        }
      )
    end

    on Transfer::Events::MoneyDeposited do |event|
      @id = id
      @target = event.data.fetch(:target)
      @requested_at = event.data.fetch(:requested_at)
      @status = :completed
    end

    on Transfer::Events::MoneyWithdrawn do |event|
      @id = id
      @source = event.data.fetch(:source)
      @requested_at = event.data.fetch(:requested_at)
      @status = :completed
    end
  end
end
