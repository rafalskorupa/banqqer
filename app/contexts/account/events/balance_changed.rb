# frozen_string_literal: true

module Account
  module Events
    class BalanceChanged < RailsEventStore::Event; end
  end
end
