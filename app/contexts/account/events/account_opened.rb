# frozen_string_literal: true

module Account
  module Events
    class AccountOpened < RailsEventStore::Event; end
  end
end
