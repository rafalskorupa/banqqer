# frozen_string_literal: true

module Account
  module Handlers
    class BuildAccountProjection < ApplicationJob
      prepend RailsEventStore::AsyncHandler

      def perform(event)
        account_number = event.data.fetch(:account_number)

        link_to_stream(event, account_number)

        payload = calculate_state(account_number)

        account = Account::Projection.find_or_initialize_by(account_number: params[:account_number])
        account.status = params[:status]
        account.balance = params[:balance]
        account.opened_at = params[:opened_at]

        account.save!
      end

      private

      def calculate_state(account_number)
        params = RailsEventStore::Projection
                 .from_stream(stream_name(account_number))
                 .init(-> { { account_number: account_number, balance: 0 } })
                 .when(
                   Account::Events::AccountOpened, lambda do |state, event|
                                                     state[:opened_at] = event.data.fetch(:open_date)
                                                     state[:status] = "open"
                                                   end
                 ).when(
                   Account::Events::BalanceChanged, lambda do |state, event|
                                                      state[:balance] = state[:balance] + event.data.fetch(:balance_chage)
                                                    end
                 )
                 .run(Rails.configuration.event_store)
      end

      def link_to_stream(event, account_number)
        Rails.configuration.event_store.link(
          event.event_id,
          stream_name: stream_name(account_number)
        )
      rescue RubyEventStore::EventDuplicatedInStream
      end

      def stream_name(account_number)
        Account::Repository.new.stream_name(account_number)
      end
    end
  end
end
