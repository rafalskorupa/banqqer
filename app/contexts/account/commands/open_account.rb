# frozen_string_literal: true

module Account
  module Commands
    class OpenAccount
      def self.call!(account_number)
        new.call!(account_number)
      end

      def call!(account_number)
        account_repository.with_aggregate(account_number) do |account|
          account.open(account_number)
        end
      end

      private

      def account_repository
        @account_repository = Account::Repository.new
      end
    end
  end
end
