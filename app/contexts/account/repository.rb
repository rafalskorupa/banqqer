# frozen_string_literal: true

module Account
  class Repository < AggregateRepository
    aggregate Account::Aggregate
  end
end
