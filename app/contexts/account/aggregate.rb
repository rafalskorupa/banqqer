# frozen_string_literal: true

module Account
  class Aggregate
    class HasBeenAlreadyOpened < StandardError; end
    class NegativeBalanceNotAllowed < StandardError; end

    include AggregateRoot

    attr_reader :number, :balance, :status, :id

    def initialize
      @status = :new
      @balance = 0
    end

    def active?
      status != :new
    end

    def open(account_number)
      raise HasBeenAlreadyOpened if status != :new

      apply Account::Events::AccountOpened.new(
        data: {
          account_number: account_number,
          open_date: DateTime.now.iso8601
        }
      )
    end

    def transfer(transfer_id, balance_change)
      raise NegativeBalanceNotAllowed if (balance + balance_chage).negative?

      apply Account::Events::BalanceChanged.new(
        data: {
          account_number: number,
          transfer_id: transfer_id,
          balance_change: balance_change,
          transfer_date: DateTime.now.iso8601
        }
      )
    end

    on Account::Events::AccountOpened do |event|
      @number = event.data.fetch(:account_number)
      @open_date = event.data.fetch(:open_date)
      @status = :open
    end

    on Account::Events::BalanceChanged do |event|
      @balance = balance + event.data.fetch(:balance_change)
    end
  end
end
