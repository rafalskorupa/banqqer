# frozen_string_literal: true

module Account
  class Projection < ApplicationRecord
    def self.write(params)
      ActiveRecord::Base.transaction do
        # advisory_lock(params[:account_number])

        account = find_or_initialize_by(account_number: params[:account_number])

        account.status = params[:status] || account.status
        account.balance = params[:balance]
        account.opened_at = params[:opened_at]

        open.save!
      end
    end
  end
end
